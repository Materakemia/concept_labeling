# -*- coding: utf-8 -*-
"""
@author: 
"""

#CBOW Word2Vectpr model training with combined Treebank and vulnerability data sets
from cbowSEmbedding import CbowSEmbedding
from text_sequence_input import TextSequenceInput as Input, WordEmbeddingPresenterFactory, RowTextFile as VulnerabilityData, CbowInputPresenter
from posInput import TreebankInput
from training import Trainer
import constants
import numpy as np

def main():
    treeBankPath = None
    vulnerabilityPath = r"Corpus/190601_Corpus.txt"
    treeBankCorpus,_ = TreebankInput(treeBankPath).getCorpus()
    vulnerabilityCorpus = VulnerabilityData(vulnerabilityPath).getCorpus()
    corpus = treeBankCorpus + vulnerabilityCorpus
    input = Input(corpus, (.7,.1,.2), WordEmbeddingPresenterFactory, isLabeled=False, includeCharacterEncoding=False)
    input.initializeEmptyTextSequenceInput()
    dictionarySize = input.getWordDictionarySize()
    wordEmbeddingDimension = constants.VECTOR_DIMENSION
    batchSize = constants.BATCH_SIZE
    windowSize = constants.W2V_WINDOW_SIZE
    print("Word Dictionary: \t\t\t{}".format(dictionarySize))
    print("Word Embedded Vectors Dimension: \t{}".format(wordEmbeddingDimension))
    print("Batch Size: \t\t\t\t{}".format(batchSize))
    print("Context window size: \t\t{}".format(windowSize))
    model = CbowSEmbedding(dictionarySize, wordEmbeddingDimension, window = windowSize).getModel()
    
    trainingPresenter = CbowInputPresenter(
                input.getTrainingData(), #W2V Presenter
                batchSize,
                windowSize, #W2V Context Window
                dictionarySize #The combined dictionary of treebank and vulnerability corpus
            )
            
    validationPresenter = CbowInputPresenter(
                input.getValidationData(),
                batchSize,
                windowSize,
                dictionarySize
            )
    testPresenter = CbowInputPresenter(
                input.getTestData(),
                batchSize,
                windowSize,
                dictionarySize
            )
    epochs = 75

    training = Trainer(trainingPresenter, validationPresenter, testPresenter, model, epochs, statPath="TrainingStat")   
    training.train()    
    testBatches = testPresenter.nBatches()
    model.evaluate_generator(testPresenter, steps = testBatches)
    embedding = model.get_layer("embedding")
    embeddingWeights = embedding.get_weights()[0]
    np.save(r'TrainedModels/CBOWWeights.npy', embeddingWeights)
    
        # save also the partitioned dataset to file so that evaluation is done on the saved models
    input.save("Dataset/cbow_data_set")

if __name__ == '__main__':
    main()

