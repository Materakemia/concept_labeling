# -*- coding: utf-8 -*-
"""
Created on Sun Jul 28 13:36:25 2019

@author: 
"""

'''
Enumerates all posible phrases in the sentence
The sentence shall be parsed with a grammer parser and be
presented as a bracketed structure
'''
import nltk
from io import StringIO
from interfaces import Input, InputPresenter 
import numpy as np
import constants
from text_sequence_input import DefaultTextEncoder
import math
import re
import collections
import itertools

class PhraseEnumerator:
    def __init__(self, sentence):
        assert (sentence != None)
        assert (sentence != "")
        self._sentence = sentence
        self._stack = []
        self._phrases = []
        self._collectedPhrases = []
        self._nonTerminalDict = {}
        self._parse()
        self._collectNounPhrases()
        
    def _parse(self):
        #trimming leading and ending spaces
        self._sentence = self._sentence.strip()
        i = 0
        self._resetAccumulator()
        phrase = None
        checkPhrase = False
        while i < (len(self._sentence)-1):
            c = self._sentence[i]
            if c=='(':
                self._accumulator = self._readCFGTag(i+1)
                i+=len(self._accumulator)
                self._push(self._getAccumulator())
            elif c==' ':
                terminal = self._readTerminal(i+1)
                phrase = tuple([self._getLastValue(), terminal])
                i+=len(terminal)
                checkPhrase = True
            elif c==')':
                parent = self._pop()
                if checkPhrase:
                    path = self._getPrefix(parent)
                    phrase = tuple([path, phrase[1]])
                    self._phrases.append(phrase)
                    checkPhrase = False
            else:
                if(not c.isspace()):
                    self._appendToAccumulator(c)
            i+=1
    
    def _appendToAccumulator(self, chr):
        self._accumulator += chr
        
    def _getAccumulator(self):
        nonTerminal = self._accumulator
        if nonTerminal in self._nonTerminalDict:
            self._nonTerminalDict[nonTerminal]+=1
        else:
            self._nonTerminalDict[nonTerminal]=0
        nonTerminal = nonTerminal + str(self._nonTerminalDict[nonTerminal])
        self._resetAccumulator()
        return nonTerminal
                    
    def _resetAccumulator(self):
        self._accumulator = ""
        
    def _readCFGTag(self, location):        
        endSpace = self._sentence.find(' ', location)
        endBrace = self._sentence.find('(', location)
        end = endSpace if endSpace < endBrace else endBrace 
        return self._sentence[location:end]
        
    def _readTerminal(self, location):
        end = self._sentence.find(')',location)
        return self._sentence[location:end]
    
    def _push(self, acc):
        self._stack.append(acc)
        
    def _pop(self):
        return self._stack.pop();
    
    def _getLastValue(self):
        return self._stack[-1]
    
    def _getPrefix(self, nonTerminal):
        return '-'.join(self._stack) + "-"+nonTerminal
    
    def _searchPhrase(self, parent):
        parentPath = self._getPrefix(parent)
        filtered = [terminal for phrase, terminal in self._phrases if self._isImmediateChild(phrase, parentPath)]
#        filtered = [terminal for phrase, terminal in self._phrases if self._isImmediateChild(phrase, parentPath)]
        return " ".join(filtered)

    def _isImmediateChild(self, phrase, parentPath):
        return phrase.startswith(parentPath) and (phrase.find("-", len(parentPath)+1)==-1)

    def _collectNounPhrases(self):
        tag = 'NP'
        noOfNounPhrases = self._nonTerminalDict.get(tag)
        if noOfNounPhrases is not None:
            noOfNounPhrases += 1
            for tagSuffix in range(noOfNounPhrases):
               tagWithSuffix = '-' + tag + str(tagSuffix) + '-'
               terminals = [terminal for path, terminal in self._phrases if (tagWithSuffix  in path)]
               if len(terminals) > 0:
                   tagPaths = [path for path, terminal in self._phrases if (tagWithSuffix  in path)]
                   index = (tagPaths[0].find(tagWithSuffix) + len(tagWithSuffix)-1)
                   tagPath = tagPaths[0][:index]
                   tagTerminals = " ".join(terminals)
                   nounPhrases = (tagPath, tagTerminals)
                   self._collectedPhrases.append(nounPhrases)
           
    def getPhrases(self):
        return self._phrases
    
    def getCollectedNounPhrases(self):
        return self._collectedPhrases
    
class GazetteInput(Input):
    def __init__(self, 
                 gazette, 
                 partitionRatio,
                 presenterFactory,
                 charEncoder = DefaultTextEncoder(),
                 characterVocabularySize = constants.CHARACTER_VOCABULARY_SIZE,
                 **kwargs):
        self._partitionRatio = partitionRatio
        self._charEncoder = charEncoder
        self._presenterFactory = presenterFactory
        self._characterVocabularySize = characterVocabularySize
        sequence = [[char.lower() for char in entry] for entry in gazette]
        self.encodeSequence(sequence)
        self.reset()

        super().__init__(**kwargs)
        
    def reset(self):
        bins = np.random.choice(3, len(self._characterSequence), p=list(self._partitionRatio))
        self._trainingData = np.array(np.where(bins==0)[0],dtype=np.int32)
        self._validationData = np.where(bins==1)[0]
        self._testData = np.where(bins==2)[0]
        
    def encodeSequence(self, sequence):
        theAlphabet = list(itertools.chain.from_iterable(sequence))
        theAlphabetFrequencies = collections.Counter(theAlphabet).most_common(self._characterVocabularySize-2)

        charVocabulary = [[constants.PADDING,1],[constants.UNKNOWN, 1]]
        charVocabulary.extend([[element,frequence] for element,frequence in theAlphabetFrequencies])
        self._dictionary, self._reverseDictionary = self._charEncoder.encode(charVocabulary)
        encodedSequence = []
        for entry in sequence:
            encodedEntry = []
            for character in entry:
                try:
                    encodedEntry.append(self._dictionary[character])
                except:
                    encodedEntry.append(constants.UNKNOWN_CODE)
            encodedSequence.append(encodedEntry)
        self._characterSequence = encodedSequence
        
    def getTrainingData(self):
        return self._presenterFactory(self._trainingData, self,lambda x: self._characterSequence[x])
    
    def getTestData(self):
        return self._presenterFactory(self._trainingData, self,lambda x: self._characterSequence[x])
    
    def getValidationData(self):
        return self._presenterFactory(self._trainingData, self,lambda x: self._characterSequence[x])
    
    def save(self, path):
        return
    
    def load(self, path):
        return
    
    def getGazette(self):
        gazette = [np.pad(np.array(entry),(0,constants.MAX_PHRASE_SIZE-len(entry)),
              'constant', constant_values=(0, constants.PADDING_CODE)) for entry in self._characterSequence]

        return np.array(gazette)
    
    def getCharacterVocabularySize(self):
        return len(self._dictionary)-2;
    
class GazettePresenter(InputPresenter):
    def __init__(self, batchSize, gazette, sourceIndices, 
                 randomSamples, sourceAccessor, maxPhraseLength,
                 charVocabularySize,  **kwargs):
        assert(batchSize < constants.BUFFER_SIZE)
        assert(randomSamples > 1)
        
        #How many enteries in the whole epoch
        self._nElements =  int(len(sourceIndices)*(1+randomSamples))
        self._sourceIndices = sourceIndices
        self._randomSamples = math.ceil(randomSamples)
        self._sourceAccessor = sourceAccessor
        self._maxPhraseLength = maxPhraseLength
        self._charVocabularySize = charVocabularySize
        self._gazette = np.array([np.pad(
                entry,
                (0,maxPhraseLength-len(entry)),
                'constant',
                constant_values=(0, constants.PADDING_CODE)) for entry in gazette])
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()

        super().__init__(batchSize, **kwargs)

    def _fetch_(self):
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        x = []; y = []
        for xi,yi in batch:
            x.append(xi); y.append(yi);#g.append(self._gazette)
        return [np.array(x)], np.array(y)
    
    def nBatches(self):
        return self._nElements//self._batchSize + 1
    
    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
           if(self._currentSequence < len(self._sourceIndices)):
               sourceIndex = self._sourceIndices[self._currentSequence]
               #Exactly matching sample
               entry_1 = self._sourceAccessor(sourceIndex)
               paddedEntry_1 = np.pad(entry_1, (0,self._maxPhraseLength-len(entry_1)),
                                      'constant', constant_values=(0, constants.PADDING_CODE))
#               paddedEntry_1.resize((1,self._maxPhraseLength))
               paddedEntry_1 = (paddedEntry_1,1)
               #Partialy or not matching samples
               randomSamples = self._generateRandomSamples_(entry_1)
               randomSamples.append(paddedEntry_1)
               entry_0_Size = np.random.randint(0, self._maxPhraseLength)
               entry_0 = np.random.randint(0, self._charVocabularySize+1, entry_0_Size)
               paddedEntry_0 = np.pad(entry_0,(0,self._maxPhraseLength-len(entry_0)),
                                       'constant', constant_values=(0, constants.PADDING_CODE))
#               paddedEntry_0.resize((1,self._maxPhraseLength))
               paddedEntry_0 = (paddedEntry_0,0)
               randomSamples.append(paddedEntry_0)
               self._buffer.extend(randomSamples)
               self._currentSequence += 1
           else:
               break
           
    def _generateRandomSamples_(self, entry):
       randVals = np.random.random_sample(self._randomSamples)
       randMin = min(randVals); randMax = max(randVals)
       ymax = len(entry)/(len(entry)+1); ymin = len(entry)/self._maxPhraseLength
       genVals = [(x-randMin)*(ymax - ymin)/(randMax-randMin)+ymin for x in randVals]
       genLengths = [self._getRandomLength_(x,len(entry)) for x in genVals]
       prefixLen = [np.random.randint(0,l) for l in genLengths]
       return [(self._generatePartialSample_(prefixLen[i],genLengths[i]-prefixLen[i], entry),genVals[i]) for i in range(len(randVals))]
       
    def _getRandomLength_(self,val,lEntry):
        totalLength = math.floor(lEntry/val);
        if(totalLength <= lEntry):
            return 1
        else:
            return totalLength - lEntry
        
    def _generatePartialSample_(self, prefixLength, suffixLength, entry):
       sample = list(np.random.randint(1,self._charVocabularySize+1, prefixLength))
       sample.extend(list(entry))
       sample.extend(list(np.random.randint(1, self._charVocabularySize+1, suffixLength)))
       sample = np.array(sample)
       
       paddedSample = np.pad(sample, (0,self._maxPhraseLength-len(sample)),
                                      'constant', constant_values=(0, constants.PADDING_CODE))
#       paddedSample.resize((1,self._maxPhraseLength))
       return paddedSample
       
def GazettePresenterFactory(indices, gazetteInput, sequenceAccessor):
    print(len(indices))
    return GazettePresenter(10, gazetteInput.getGazette(), indices, constants.GAZETTE_NEGATIVE_SAMPLES, 
                            sequenceAccessor, constants.MAX_PHRASE_SIZE, gazetteInput.getCharacterVocabularySize());

class GazetteReader():
    def __init__(self, path):
        self._gazette = [line.rstrip('\n') for line in open(path)]
        
    def getGazette(self):
        return self._gazette
    
def main():
    '''
    groucho_grammar = nltk.CFG.fromstring("""S -> NP VP
                                          PP -> P NP
                                          NP -> Det N | Det N PP | 'I'
                                          VP -> V NP | VP PP
                                          Det -> 'an' | 'my'
                                          N -> 'elephant' | 'pajamas'
                                          V -> 'shot'
                                          P -> 'in'
                                          """)
    sent = ['I', 'shot', 'an', 'elephant', 'in', 'my', 'pajamas']
    parser = nltk.ChartParser(groucho_grammar)
    out = StringIO()
    for tree in parser.parse(sent):
        print(tree, file=out)   # 'out' behaves like a file
        break
    parsedSentence = out.getvalue()
    listChars = [c for c in parsedSentence]
    enumrator = PhraseEnumerator(parsedSentence)
    phrases = enumrator.getPhrases()
    print(phrases)
    '''
    
    gazetteInput = GazetteInput(["parsedSentence = out.getvalue()",
            "listChars = [c for c in parsedSentence]",
            "enumrator = PhraseEnumerator(parsedSentence)",
            "phrases = enumrator.getPhrases()",
            "print(phrases)"],
        (0.8,0.05,0.15), GazettePresenterFactory)
#    trainingPresenter = gazetteInput.getTrainingData()
    gazette = gazetteInput.getGazette()
    print(gazette)
    
if __name__ == '__main__':
    main()