# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 18:15:23 2019

@author: 
"""
import numpy as np
from interfaces import InputPresenter, SequencialInputPresenter, Input

#Memory based batch input presenter
class MemoryBatchPresenter(InputPresenter):
    def __init__(self, batchSize, source):
        self._source = source
        self._current = 0
        self._sourceSize = source.shape[0]
        super().__init__(batchSize)
        
    def _fetch_(self):
        index = self._current*self._batchSize
        if self._batchSize < self._sourceSize - index:
            self._current += 1
            return self._source[index:index+self._batchSize]
        self._current = 0
        return self._source[index:]
    
    def nBatches(self):
        return self._sourceSize//self._batchSize + 1

#Memory based sequencial input presenter
class MemorySequencialInputPresenter(SequencialInputPresenter):
    '''The composing Input object shall instantiate a 
    MemorySequentialInputPresenter object with the corpus and a list of slice 
    objects representing the sequences included in the particular dataset. 
    The client object accesses the underlying vectors in windows of predefined 
    size the presenter provides vectors of the window size sliding the window to 
    the right each time the getNextWindow method is called. The returned window 
    is paded with zero vectors at the approprite side. '''
    def __init__(self, source, sequences, **kwargs):
        super().__init__(**kwargs)
        self._source = source
        self._currentStep = 0
        self._currentSequenceIndex = 0
        self._sequencesIndices = sequences
        self._nextSequence()
       

    def getNextWindow(self, windowSize, forward=True):
        if(self._currentStep >= len(self._currentSequence)+windowSize-1):
            self._currentStep = 0
            self._nextSequence()
        step = self._currentStep
        self._currentStep += 1
        sequenceSize = len(self._currentSequence)
        leftFillSize = step - windowSize + 1
        start = step-windowSize+1
        if(leftFillSize > 0):
            leftFillSize = 0
        else:
            start = 0
        rightFillSize = step - sequenceSize+1
        if(rightFillSize < 0):
            rightFillSize = 0
        else:
            start = sequenceSize + rightFillSize - windowSize
        fillSize = leftFillSize + rightFillSize
        if(fillSize == 0):
            return self._currentSequence[start:start+windowSize]
        else:
            fillShape = tuple([abs(fillSize)] + list(self._currentSequence.shape[1:]))
            fill = np.zeros(fillShape, self._source.dtype)
            if(fillSize < 0):
                partialWindow = self._currentSequence[0:windowSize-abs(fillSize)]
                return np.concatenate((fill, partialWindow))
            else:
                partialWindow = self._currentSequence[start:]
                return np.concatenate((partialWindow, fill))
        
    def _nextSequence(self):
        if(len(self._sequencesIndices) == 0):
            self._currentSequence = self._source
        else:
            self._currentSequence = self._source[self._sequencesIndices[self._currentSequenceIndex]]
            self._currentSequenceIndex += 1
            if(self._currentSequenceIndex == len(self._sequencesIndices)):
                self._currentSequenceIndex = 0
            
            
#Memory based batchinput device            
class MemoryInput(Input):
    def __init__(self, source, shape, batchSize, partitionRatio):
        self._batchSize = batchSize
        self._source = source.reshape(shape)
        self._partitionRatio = partitionRatio
        self.reset()
       
    def reset(self):
        bins = np.random.choice(3, len(self._source), p=list(self._partitionRatio))
        self._trainingData = self._source[np.where(bins==0)[0]]
        self._validationData = self._source[np.where(bins==1)[0]]
        self._testData = self._source[np.where(bins==2)[0]] 
        
    def getTrainingData(self):
        return MemoryBatchPresenter(self._batchSize, self._trainingData)
    
    def getTestData(self):
        return MemoryBatchPresenter(self._batchSize, self._testData)
    
    def getValidationData(self):
        return MemoryBatchPresenter(self._batchSize, self._validationData)

#Memory based sequenction input device            
class MemorySequentialInput(Input):
    '''The device captures the data corpus into the memory and partitions
    the corpus in accordance with the partionRatio tuple of format (d,v,t),
    where d,v and t are percentages of training, validation and test datasets,
    respectively. The data corpus is assummed to be sequential with a token
    provided as initialization input separating the sequences in the corpus. 
    The device recognize a sequence as a unit. Partitioning is thus conducted
    by distributing sequences accross the training, validation and test datasets.
    If the sequenceDelimiter is None partitioning is carried out as in block
    input devices but sequential presenters are offered initialized with empty
    sequence indices.'''
    def __init__(self, corpus, partitionRatio, sequenceDelimiter=None, **kwargs):
        super().__init__(**kwargs)
        self._corpus = corpus
        self._partitionRatio = partitionRatio
        self._sequenceDelimiter = sequenceDelimiter
        if(sequenceDelimiter != None):
            corpusShapeSize = 1
            for axisSize in corpus.shape:
                corpusShapeSize *= axisSize
            step = int(corpusShapeSize/corpus.shape[0])
            delimiterLocations = np.where(corpus == sequenceDelimiter)[0]
            delimiterLocations = delimiterLocations[0:len(delimiterLocations):step]
            locations = len(delimiterLocations)
            #We shall define _corpusUnitsIndices as slice with the range between
            #two subsequent delimiters
            if(locations == 0):
                self._corpusUnitsIndices = []
            else:
                #The first sequence starts with the corpus and ends before the 
                #first delimiter
                self._corpusUnitsIndices = [slice(0, delimiterLocations[0])];
                #subsequent sequences start with the next vector from a delimiter
                self._corpusUnitsIndices += [slice(delimiterLocations[i-1]+1, delimiterLocations[i]) \
                                               for i in range(1, locations)]
                #the last sequence starts with the vector next to the last delimiter
                #runs to the end of the corpus
                self._corpusUnitsIndices.append(slice(delimiterLocations[locations-1]+1,len(corpus)))
                self._corpusUnitsIndices = np.array(self._corpusUnitsIndices)
        self.reset()
    
    def reset(self):
        #generating random set of indexes to bins corresponding to the training, validation and test datasets
        #with probablities matching their respective ratios. The generated bin index determines 
        #where the sequence will be put
        bins = np.random.choice(3, len(self._corpusUnitsIndices), p=list(self._partitionRatio))
        trainingData = self._corpusUnitsIndices[np.where(bins==0)]
        validationData = self._corpusUnitsIndices[np.where(bins==1)]
        testData = self._corpusUnitsIndices[np.where(bins==2)]
        self._trainingPresenter = MemorySequencialInputPresenter(self._corpus, trainingData)
        self._validationPresenter =  MemorySequencialInputPresenter(self._corpus, validationData)
        self._testPresenter =  MemorySequencialInputPresenter(self._corpus, testData)

    def getTrainingData(self):
        return self._trainingPresenter
        
    def getTestData(self):
        return self._validationPresenter
        
    def getValidationData(self):
        return self._validationPresenter

        
    

