﻿#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 09:43:54 2021

@author: """

import numpy as np
from sklearn import datasets
from sklearn.metrics import pairwise_distances
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from matplotlib import colors
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from math import pi
#try DBScan
from sklearn.cluster import DBSCAN
from sklearn.pipeline import Pipeline
# %pip install opentsne
# from openTSNE import TSNEEmbedding
# from openTSNE.affinity import PerplexityBasedNN
# from openTSNE import initialization
# from openTSNE.callbacks import ErrorLogger

#from cuml import TSNE as cumlTSNE
#from cuml import PCA as cumlPCA
#import cudf
from sys import exit

#from mnist import MNIST

# the path should point to the FIt-SNE directory
import sys; sys.path.append('FIt-SNE/')
from fast_tsne import fast_tsne
#from openTSNE import TSNE

from sklearn.model_selection import KFold
from sklearn.metrics import f1_score, accuracy_score, recall_score, precision_score
#from sys import exit
#from itertools import permutations

def get_clusters( dataset, eps_multiplier=1, late_exag=False ):
    #
    #scale the data
#     scaler = StandardScaler()
#     dataset = scaler.fit_transform(dataset)
    
    #fast tsne
    # Do PCA and keep 50 dimensions
    X = np.array(dataset) - np.array(dataset).mean(axis=0)
    U, s, V = np.linalg.svd(dataset, full_matrices=False)
    X50 = np.dot(U, np.diag(s))[:,:50]
    if late_exag:
        X_embed = fast_tsne(X50, late_exag_coeff=4)
    else:
        X_embed = fast_tsne(X50)
#    X_embed_opentsne = TSNE().fit(X50)
#    X_embed = X_embed_opentsne
    #
    #get pairwise distances of embedding, set zeros to inf for min function to follow
#    pair_dist = pairwise_distances(X_embed)

    #
    #create the clusters
    clusters = []

    #
    #set epsilon
#    d = np.mean(pair_dist) * eps_multiplier

    #
    #DBSCAN
    dbscan = DBSCAN(eps=1, min_samples=2)#int(dataset.shape[0]*0.015))
    clustering = dbscan.fit( X_embed )
    for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
        clusters.append( np.argwhere( clustering.labels_ == i ).flatten().tolist() )
    
    del dbscan
    del clustering

    clusters.sort(key=len)
    return X_embed, clusters[::-1]




def get_explanatory_importances(dataset,clusters,feature_names):
    #
    #infer labels from clusters
    labels = np.zeros(dataset.shape[0])
    for idx, cluster in enumerate( clusters ):
        labels[cluster] = idx

    #
    #scale the data
    #Fit RF and get predictions
    rf = Pipeline([('scaler',StandardScaler()),('rf',RandomForestClassifier())])
    rf.fit( dataset, labels )
    predictions = rf.predict( dataset )

    #
    #Show the important features

    return sorted(zip(rf['rf'].feature_importances_,feature_names))[::-1], predictions, rf

#
#
#def get_epsilon( this_dataset, lower_limit=1 ):
#    folds = KFold(n_splits=5, shuffle=False, random_state=None)
#
#    eps_list = []
#    f1_list = []
#    
#    for eps_multiplier in np.linspace(0.001,0.5,10):
#        f1_scores = []
#        accs = []
#        precisions = []
#        recall = []
#
#        X_embedded, clusters = get_clusters( this_dataset, eps_multiplier )
#        
#        if len(clusters) <= lower_limit:
#            continue
#            
#        sorted_list, predictions, rf = get_explanatory_importances(this_dataset, clusters, feature_names)
#        
#        
#
#        for train_index, test_index in folds.split(this_dataset):
#            train_data, test_data = np.array(this_dataset)[train_index], np.array(this_dataset)[test_index]
#            X_embedded1, clusters1 = get_clusters( train_data, eps_multiplier )
#            
#            sorted_list1, predictions1, rf1 = get_explanatory_importances(train_data, clusters1, feature_names)
#
#            best_perm = []
#
#            for c1 in clusters1:
#                best_sum = 0
#                best_cluster = None
#                for idx, c in enumerate(clusters):
#                    this_sum = len(list(set(c) & set(train_index[c1])))
#                    if this_sum > best_sum and idx not in best_perm:
#                        best_sum = this_sum
#                        best_cluster = idx
#                best_perm.append(best_cluster)
##             print(best_perm)
#
#            rf1_predictions = rf1.predict(test_data)
#            rf_predictions = rf.predict(test_data)
#
#            rf_predictions = list(map(lambda x: best_perm.index(x) if x in best_perm else 0, rf_predictions))
#
#
#            f1_scores.append( f1_score(rf_predictions, rf1_predictions, average='weighted' ) )
#            accs.append( accuracy_score(rf_predictions, rf1_predictions, normalize=True ) )
#            precisions.append( precision_score(rf_predictions, rf1_predictions, average='weighted' ) )
#            recall.append( recall_score(rf_predictions, rf1_predictions, average='weighted' ) )
##        print(np.mean(accs), np.mean(precisions), np.mean(recall), np.mean(f1_scores))
#        
#        eps_list.append( eps_multiplier )
#        f1_list.append( np.mean( f1_scores ) )
#        
#    return np.mean( np.array(eps_list)[np.where(f1_list == np.max(f1_list))] )




def evaluate(path, labelPath):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):
    # serialize weights to HDF5
    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")





#import numpy as np
#import matplotlib.pyplot as plt

from autoLabelerInput import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory
from AutoLablerGveaModel import AutoLabelerGvaeModel
#import tsne_dbscan_rf as tdr
import sys; sys.path.append('/home/hanlinyi/下载/VAE_Code/FIt-SNE')
import constants
#from fast_tsne import fast_tsne
#intermediate_dim = 599
#latent_dim = 200
#M = 3
#N = 50
#path = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample_0.8.8.3_two.xml'
#labelPath = 'Dataset/20201129_LabledEntities.csv'
#epochs = 1
#batch_size = constants.BATCH_SIZE
#
#corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
#inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
#inData.initializeEmptyPathInput()
#
#(x_train, _), (x_test, y_test) = _getLabeledInput(inData)
#
#absDictionarySize = inData.getPathDictionarySize()
#relDictionarySize = inData.getRelativePathDictionarySize()
#
## train the auto-labeler
#provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
#model = provider.getModel()
#encoder = provider.getEncoder()
#for e in range(epochs):
#    model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
#            validation_data=(x_test, x_test))
#    provider.updateTau(e)
#    
## abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
## rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
## encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))
#
## tsne = TSNE(metric='hamming')
## embedding_train = tsne.fit(x_train)
## viz = embedding_train.transform(encoded_test)
#
## plt.figure(figsize=(6,6))
## plt.scatter(viz[:,0], viz[:, 1],c=y_test)
## plt.colorbar()
## plt.show()
#from sklearn.metrics import pairwise_distances
#
#abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
#rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
#encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))
#dataset = encoded_train
#U, s, V = np.linalg.svd(dataset, full_matrices=False)
#X50 = np.dot(U, np.diag(s))[:,:50]
#X_embed = fast_tsne(X50, late_exag_coeff=4)
#a = X_embed[:20000]
#pair_dist = pairwise_distances(a)


#X_embed.shape





#def train(
#        path, # spefify the path to the xml file containing the selected for training noun-phrases
#        labelPath # specify the path to the csv file containing noun-phrase to label mapping
#        ):
"""    
Trains the AutoLabelerGveaModel with the provided training data
Parameters
----------
path : TYPE. string TYPE.
    DESCRIPTION.
Spefify the path to the xml file containing the selected for training noun-phrases 
    DESCRIPTION.
labelPath: TYPE. string TYPE.
    DESCRIPTION.
specify the path to the csv file containing noun-phrase to label mapping
Returns
-------
None.

"""
path = 'Dataset/gazette_data_set_relative_path_noun_phrases_NotLabled_15.xml'
labelPath = 'Dataset/20201129_LabledEntities.csv'
intermediate_dim = 599
latent_dim = 200
M = 3
N = 50

epochs = 50
batch_size = constants.BATCH_SIZE

corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
inData.initializeEmptyPathInput()

(x_train, _), (x_test, y_test) = _getLabeledInput(inData)

absDictionarySize = inData.getPathDictionarySize()
relDictionarySize = inData.getRelativePathDictionarySize()

# train the auto-labeler
provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
model = provider.getModel()
encoder = provider.getEncoder()
for e in range(epochs):
    model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
            validation_data=(x_test, x_test))
    provider.updateTau(e)
    
# abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
# rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
# encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))

# tsne = TSNE(metric='hamming')
# embedding_train = tsne.fit(x_train)
# viz = embedding_train.transform(encoded_test)

# plt.figure(figsize=(6,6))
# plt.scatter(viz[:,0], viz[:, 1],c=y_test)
# plt.colorbar()
# plt.show()
abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))

#X_embedded, clusters = tdr.get_clusters( encoded_train, 0.0055, late_exag=True )









#from sklearn.metrics import pairwise_distances

dataset = encoded_train
#U, s, V = np.linalg.svd(dataset, full_matrices=False)
#X50 = np.dot(U, np.diag(s))[:,:50]
#
#
#
#
#
#
#
#
#
#from sklearn.datasets import fetch_openml
#mnist = fetch_openml('mnist_784')
#X = mnist.data
#y = mnist.target.astype('int')
#print(X)
#dataset = np.array(X)#np.random.choice(len(train_images), 30000)]
np.save("dataset_accuracy(vae+fast+dbscan).npy",dataset)

del abs_encoded_train,corpus,encoded_train,rel_encoded_train,x_test,x_train
del y_test
#dataset = np.load("dataset_accuracy.npy")
feature_names = range(dataset.shape[1])
#np.random.shuffle(dataset)
#dataset = dataset[0:20000]
kk = 0
f1_scores = [2]
accs = []
precisions = [1]
recall = []
while kk <10 and (np.mean(precisions)<=np.mean(f1_scores) or np.mean(f1_scores)<=np.mean(recall)):
    kk += 1
    folds = KFold(n_splits=5, shuffle=False, random_state=None)
    
    f1_scores = []
    accs = []
    precisions = []
    recall = []
    eps_params_used = []
    
    import tracemalloc
    
    tracemalloc.start()
    
    for train_index, test_index in folds.split(dataset):
        train_data, test_data = np.array(dataset)[train_index], np.array(dataset)[test_index]
        
        eps_multiplier = 0.0055 #get_epsilon(train_data, 5)
    #    print('eps %s' % eps_multiplier)
        eps_params_used.append( eps_multiplier )
        
        X_embedded, clusters = get_clusters( dataset, eps_multiplier, late_exag=True )
    
        sorted_list, predictions, rf = get_explanatory_importances(dataset, clusters, feature_names)
        
        ##
        snapshot = tracemalloc.take_snapshot()
        top_stats = snapshot.statistics('lineno')
    
    #    print("[ Top 20 ]")
    #    for stat in top_stats[:20]:
    #        print(stat)
        ##
    
        X_embedded1, clusters1 = get_clusters( train_data, eps_multiplier, late_exag=True )
        sorted_list1, predictions1, rf1 = get_explanatory_importances(train_data, clusters1, feature_names)
    
        best_perm = []
    
        for c1 in clusters1:
            best_sum = 0
            best_cluster = None
            for idx, c in enumerate(clusters):
                this_sum = len(list(set(c) & set(train_index[c1])))
                if this_sum > best_sum and idx not in best_perm:
                    best_sum = this_sum
                    best_cluster = idx
            best_perm.append(best_cluster)
    #     print(best_perm)
    
        rf1_predictions = rf1.predict(test_data)
        rf_predictions = rf.predict(test_data)
    
        rf_predictions = list(map(lambda x: best_perm.index(x) if x in best_perm else 0, rf_predictions))
    
    
        f1_scores.append( f1_score(rf_predictions, rf1_predictions, average='weighted' ) )
    #    accs.append( accuracy_score(rf_predictions, rf1_predictions, normalize=True ) )
        accs.append( accuracy_score(rf_predictions, rf1_predictions, normalize=True  ) )
        precisions.append( precision_score(rf_predictions, rf1_predictions, average='weighted' ) )
        recall.append( recall_score(rf_predictions, rf1_predictions, average='weighted' ) )
        
    #    print(np.mean(accs), np.mean(precisions), np.mean(recall), np.mean(f1_scores))
        
        del X_embedded, clusters, sorted_list, predictions, rf
        del X_embedded1, clusters1, sorted_list1, predictions1, rf1
        del train_data, test_data
print(pd.DataFrame({
#    'Eps Param Used':[np.mean(eps_params_used)],
    'Mean Accuracy':[np.mean(accs)],
    'Mean Precision':[np.mean(precisions)],
    'Mean Recall':[np.mean(recall)],
    'Mean F1 Score':[np.mean(f1_scores)]
}))
autolabelerEncoderModelFileName = "ALEncoder_model"
_saveModelWeights(encoder, autolabelerEncoderModelFileName)  
#print(np.mean(eps_params_used), np.mean(accs), np.mean(precisions), np.mean(recall), np.mean(f1_scores))

#X_embedded, clusters = get_clusters( dataset, 0.0055, late_exag=True )
#
#dotsize = 1
#
#fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
#fig.set_figwidth(12)
#ax1.scatter(X_embedded[:,0],X_embedded[:,1], alpha=1, s=dotsize)
#ax1.set_title('t-SNE Embedding of Data', fontsize=20)
#
#for idx, cluster in enumerate( clusters ):
#    if idx < 4:
#        ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,label="cluster %s" % idx, s=dotsize)
#    else:
#        ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,c = 'black', s=dotsize)
#
#ax2.set_title("Cluster Labels from DBSCAN", fontsize=20)
#
#ax1.axis('off')
#ax2.axis('off')
#
#lgnd = ax2.legend(bbox_to_anchor=(1.04,1), borderaxespad=0, prop={"size":12})
#for handle in lgnd.legendHandles:
#    handle.set_sizes([40.0])
#
#fig.tight_layout()
#plt.show()
