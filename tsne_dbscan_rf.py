# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 05:58:11 2021

@author: 
"""

import numpy as np
# from sklearn import datasets
from sklearn.metrics import pairwise_distances
# import matplotlib.pyplot as plt
# import seaborn as sns
# import itertools
# import pandas as pd
from sklearn.preprocessing import StandardScaler#, MinMaxScaler
# from matplotlib import colors
from sklearn.ensemble import RandomForestClassifier
# from sklearn.model_selection import train_test_split
# from math import pi
#try DBScan
from sklearn.cluster import DBSCAN
from sklearn.pipeline import Pipeline
# %pip install opentsne
# from openTSNE import TSNEEmbedding
# from openTSNE.affinity import PerplexityBasedNN
# from openTSNE import initialization
# from openTSNE.callbacks import ErrorLogger

# from cuml import TSNE as cumlTSNE
# from cuml import PCA as cumlPCA
# import cudf
# from sys import exit

# from mnist import MNIST

# the path should point to the FIt-SNE directory
import sys; sys.path.append('home/gsy/AutoLabeling_AB_20210311_Check/FIt-SNE/')

from fast_tsne import fast_tsne

from sklearn.model_selection import KFold
from sklearn.metrics import f1_score, accuracy_score, recall_score, precision_score

#
#Perform tSNE and get the embedded data
#
def get_clusters( dataset, eps_multiplier=1, late_exag=False ):
    #
    #scale the data
#     scaler = StandardScaler()
#     dataset = scaler.fit_transform(dataset)
    
    #fast tsne
    # Do PCA and keep 50 dimensions
    #X = np.array(dataset) - np.array(dataset).mean(axis=0)
    U, s, V = np.linalg.svd(dataset, full_matrices=False)
    X50 = np.dot(U, np.diag(s))[:,:50]
    if late_exag:
        X_embed = fast_tsne(X50, late_exag_coeff=4)
    else:
        X_embed = fast_tsne(X50)

    print(dataset)
    
    #
    #get pairwise distances of embedding, set zeros to inf for min function to follow
    pair_dist = pairwise_distances(X_embed)

    #
    #create the clusters
    clusters = []

    #
    #set epsilon
    d = np.mean(pair_dist) * eps_multiplier

    #
    #DBSCAN
    dbscan = DBSCAN(eps=1, min_samples=2)#int(dataset.shape[0]*0.015))
    clustering = dbscan.fit( X_embed )
    #print(clustering.labels_)
   # with open('clustering_lables.txt', 'w') as fp:
        #for l in clustering.lables_ :
           # fp.write('%s\n' % l)
            
    with open('Dataset/cluster.csv', 'w') as fp:
        fp.write('%s,%s\n' % ('cluster', 'index'))
        i=0;
        for l in clustering.labels_:
            fp.write('%d,%s\n' % (l,i))
            i = i+1

    for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
        c= np.argwhere( clustering.labels_ == i ).flatten().tolist()
        clusters.append( c )
        #print(X_embed)
        #exit()
        
    del dbscan
    del clustering

    clusters.sort(key=len)
    return X_embed, clusters[::-1]

#
#Perfrom random forest classification and show the error
#
def get_explanatory_importances(dataset,clusters,feature_names):
    #
    #infer labels from clusters
    labels = np.zeros(dataset.shape[0])
    for idx, cluster in enumerate( clusters ):
        labels[cluster] = idx

    #
    #scale the data
    #Fit RF and get predictions
    rf = Pipeline([('scaler',StandardScaler()),('rf',RandomForestClassifier())])
    rf.fit( dataset, labels )
    predictions = rf.predict( dataset )

    #
    #Show the important features

    return sorted(zip(rf['rf'].feature_importances_,feature_names))[::-1], predictions, rf

#
#technique using Cross validation for selecting the optimal epsilon
#
def get_epsilon( this_dataset, feature_names, lower_limit=1 ):
    folds = KFold(n_splits=5, shuffle=False, random_state=None)

    eps_list = []
    f1_list = []
    
    for eps_multiplier in np.linspace(0.001,0.5,10):
        f1_scores = []
        accs = []
        precisions = []
        recall = []

        X_embedded, clusters = get_clusters( this_dataset, eps_multiplier )
        
        if len(clusters) <= lower_limit:
            continue
            
        sorted_list, predictions, rf = get_explanatory_importances(this_dataset, clusters, feature_names)
        
        

        for train_index, test_index in folds.split(this_dataset):
            train_data, test_data = np.array(this_dataset)[train_index], np.array(this_dataset)[test_index]
            X_embedded1, clusters1 = get_clusters( train_data, eps_multiplier )
            
            sorted_list1, predictions1, rf1 = get_explanatory_importances(train_data, clusters1, feature_names)

            best_perm = []

            for c1 in clusters1:
                best_sum = 0
                best_cluster = None
                for idx, c in enumerate(clusters):
                    this_sum = len(list(set(c) & set(train_index[c1])))
                    if this_sum > best_sum and idx not in best_perm:
                        best_sum = this_sum
                        best_cluster = idx
                best_perm.append(best_cluster)

            rf1_predictions = rf1.predict(test_data)
            rf_predictions = rf.predict(test_data)

            rf_predictions = list(map(lambda x: best_perm.index(x) if x in best_perm else 0, rf_predictions))


            f1_scores.append( f1_score(rf_predictions, rf1_predictions, average='weighted' ) )
            accs.append( accuracy_score(rf_predictions, rf1_predictions, normalize=True ) )
            precisions.append( precision_score(rf_predictions, rf1_predictions, average='weighted' ) )
            recall.append( recall_score(rf_predictions, rf1_predictions, average='weighted' ) )
        print(eps_multiplier, np.mean(accs), np.mean(precisions), np.mean(recall), np.mean(f1_scores))
        
        eps_list.append( eps_multiplier )
        f1_list.append( np.mean( f1_scores ) )
        
    return np.mean( np.array(eps_list)[np.where(f1_list == np.max(f1_list))] )

def score_tdr(dataset, feature_names):
    folds = KFold(n_splits=5, shuffle=False, random_state=None)

    f1_scores = []
    accs = []
    precisions = []
    recall = []
    eps_params_used = []
    
    import tracemalloc
    
    tracemalloc.start()
    
    for train_index, test_index in folds.split(dataset):
        train_data, test_data = np.array(dataset)[train_index], np.array(dataset)[test_index]
        
        eps_multiplier = 0.0055 #get_epsilon(train_data, 5)
        print('eps %s' % eps_multiplier)
        eps_params_used.append( eps_multiplier )
        
        X_embedded, clusters = get_clusters( dataset, eps_multiplier, late_exag=True )
        sorted_list, predictions, rf = get_explanatory_importances(dataset, clusters, feature_names)
        
        ##
        snapshot = tracemalloc.take_snapshot()
        top_stats = snapshot.statistics('lineno')
    
        print("[ Top 20 ]")
        for stat in top_stats[:20]:
            print(stat)
        ##
    
        X_embedded1, clusters1 = get_clusters( train_data, eps_multiplier, late_exag=True )
        sorted_list1, predictions1, rf1 = get_explanatory_importances(train_data, clusters1, feature_names)
    
        best_perm = []
    
        for c1 in clusters1:
            best_sum = 0
            best_cluster = None
            for idx, c in enumerate(clusters):
                this_sum = len(list(set(c) & set(train_index[c1])))
                if this_sum > best_sum and idx not in best_perm:
                    best_sum = this_sum
                    best_cluster = idx
            best_perm.append(best_cluster)
    #     print(best_perm)
    
        rf1_predictions = rf1.predict(test_data)
        rf_predictions = rf.predict(test_data)
    
        rf_predictions = list(map(lambda x: best_perm.index(x) if x in best_perm else 0, rf_predictions))
    
    
        f1_scores.append( f1_score(rf_predictions, rf1_predictions, average='weighted' ) )
        accs.append( accuracy_score(rf_predictions, rf1_predictions, normalize=True ) )
        precisions.append( precision_score(rf_predictions, rf1_predictions, average='weighted' ) )
        recall.append( recall_score(rf_predictions, rf1_predictions, average='weighted' ) )
        
        print(eps_multiplier, np.mean(accs), np.mean(precisions), np.mean(recall), np.mean(f1_scores))
        
        del X_embedded, clusters, sorted_list, predictions, rf
        del X_embedded1, clusters1, sorted_list1, predictions1, rf1
        del train_data, test_data
    return f1_scores, accs, precisions, recall, eps_params_used
