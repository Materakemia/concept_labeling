#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def evaluate(path, labelPath):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):
    # serialize weights to HDF5
    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")





import numpy as np
import matplotlib.pyplot as plt

from autoLabelerInput import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory,AutoLableInput
from AutoLablerGveaModel import AutoLabelerGvaeModel
import tsne_dbscan_rf as tdr
import sys; sys.path.append('/home/hanlinyi/下载/VAE_Code/FIt-SNE')
import constants
from fast_tsne import fast_tsne
#intermediate_dim = 599
#latent_dim = 200
#M = 3
#N = 50
#path = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample_0.8.8.3_two.xml'
#labelPath = 'Dataset/20201129_LabledEntities.csv'
#epochs = 1
#batch_size = constants.BATCH_SIZE
#
#corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
#inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
#inData.initializeEmptyPathInput()
#
#(x_train, _), (x_test, y_test) = _getLabeledInput(inData)
#
#absDictionarySize = inData.getPathDictionarySize()
#relDictionarySize = inData.getRelativePathDictionarySize()
#
## train the auto-labeler
#provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
#model = provider.getModel()
#encoder = provider.getEncoder()
#for e in range(epochs):
#    model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
#            validation_data=(x_test, x_test))
#    provider.updateTau(e)
#    
## abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
## rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
## encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))
#
## tsne = TSNE(metric='hamming')
## embedding_train = tsne.fit(x_train)
## viz = embedding_train.transform(encoded_test)
#
## plt.figure(figsize=(6,6))
## plt.scatter(viz[:,0], viz[:, 1],c=y_test)
## plt.colorbar()
## plt.show()
#from sklearn.metrics import pairwise_distances
#
#abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
#rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
#encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))
#dataset = encoded_train
#U, s, V = np.linalg.svd(dataset, full_matrices=False)
#X50 = np.dot(U, np.diag(s))[:,:50]
#X_embed = fast_tsne(X50, late_exag_coeff=4)
#a = X_embed[:20000]
#pair_dist = pairwise_distances(a)


#X_embed.shape





#def train(
#        path, # spefify the path to the xml file containing the selected for training noun-phrases
#        labelPath # specify the path to the csv file containing noun-phrase to label mapping
#        ):
"""    
Trains the AutoLabelerGveaModel with the provided training data
Parameters
----------
path : TYPE. string TYPE.
    DESCRIPTION.
Spefify the path to the xml file containing the selected for training noun-phrases 
    DESCRIPTION.
labelPath: TYPE. string TYPE.
    DESCRIPTION.
specify the path to the csv file containing noun-phrase to label mapping
Returns
-------
None.

"""
path = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample.xml'
labelPath = 'Dataset/20201129_LabledEntities.csv'
intermediate_dim = 599
latent_dim = 200
M = 3
N = 50

epochs = 1
batch_size = constants.BATCH_SIZE

corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
inData.initializeEmptyPathInput()
f1 = np.load("x_train_1.npy")
f2 = np.load("x_train_2.npy")
x_train = [f1,f2]

absDictionarySize = inData.getPathDictionarySize()
relDictionarySize = inData.getRelativePathDictionarySize()

# train the auto-labeler
provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
model = provider.getModel()
encoder = provider.getEncoder()
_loadWeights(encoder, "ALEncoder_model")
abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))    


dataset = encoded_train
U, s, V = np.linalg.svd(dataset, full_matrices=False)
X50 = np.dot(U, np.diag(s))[:,:50]



X_embed = fast_tsne(X50, late_exag_coeff=4)


#
#get pairwise distances of embedding, set zeros to inf for min function to follow
#pair_dist = pairwise_distances(X_embed)

#
#create the clusters


#
#set epsilon
#eps_multiplier=0.0055
#d = np.mean(pair_dist) * eps_multiplier
from sklearn.cluster import DBSCAN
import pandas as pd
#
#DBSCAN
#tradition tsne
#def aa(eps,min_samples):
#    clusters = []
#    dbscan = DBSCAN(eps, min_samples)#int(dataset.shape[0]*0.015))
#    clustering = dbscan.fit( X_embed )
#    for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
#        c= np.argwhere( clustering.labels_ == i ).flatten().tolist()
#        clusters.append( c )
#        #print(X_embed)
#        #exit()
#    
#    del dbscan
#    del clustering
#    
#    clusters.sort(key=len)
#    X_embedded=X_embed
#    clusters=clusters[::-1]
#    
#    dotsize=1
#    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
#    fig.set_figwidth(12)
#    ax1.scatter(X_embedded[:,0],X_embedded[:,1], alpha=1, s=dotsize)
#    ax1.set_title('t-SNE Embedding of Data', fontsize=20)
#    color = ['red','magenta','orange','lime','black']
#    k = 0
#    slot_x = []
#    slot_y = []
#    cluster_num = []
#    index_num = []
#    for idx, cluster in enumerate( clusters ):
#        if idx in [1,2,3,4]:
#            print(idx)
#            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,label="cluster %s" % (idx), s=dotsize)
#            for i in cluster:
#                slot_x.append(X_embedded[i,0])
#                slot_y.append(X_embedded[i,1])
#                cluster_num.append(idx)
#                index_num.append(i)
#            k+=1
#        else:
#            for i in cluster:
#                slot_x.append(X_embedded[i,0])
#                slot_y.append(X_embedded[i,1])
#                cluster_num.append(-1)
#                index_num.append(i)            
#            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,c = "black",s=dotsize)
#    a = pd.DataFrame()
#    a['x'] = slot_x
#    a['y'] = slot_y
#    a['cluster'] =  cluster_num
#    a['index'] =  index_num
#    a.to_csv('./open-tsne.csv')
#    ax2.set_title("Cluster Labels from DBSCAN", fontsize=20)
#    
#    ax1.axis('off')
#    ax2.axis('off')
#    lgnd = ax2.legend(bbox_to_anchor=(1.04,1), borderaxespad=0, prop={"size":12})
#    for handle in lgnd.legendHandles:
#        handle.set_sizes([40.0])
#    
#    fig.tight_layout()
#    plt.show()
#    fig.savefig("./png/eps:"+str(eps).replace('.','-')+"+min_samples:"+str(min_samples)+".png")
#    plt.close()

#aa(1,4)
#for eps in np.arange(0.5,3.5,0.1):
#    print('eps:', eps)
#    # 迭代不同的min_samples值
#    for min_samples in range(2,30,1):   
#        print('min_samples:',min_samples)
#        aa(eps,min_samples)



#from openTSNE import TSNE
##opentsne
#X_embed_opentsne = TSNE().fit(X50)
#X_embed = X_embed_opentsne
#def aa(eps,min_samples):
#    clusters = []
#    dbscan = DBSCAN(eps, min_samples)#int(dataset.shape[0]*0.015))
#    clustering = dbscan.fit( X_embed )
#    for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
#        c= np.argwhere( clustering.labels_ == i ).flatten().tolist()
#        clusters.append( c )
#        #print(X_embed)
#        #exit()
#    
#    del dbscan
#    del clustering
#    
#    clusters.sort(key=len)
#    X_embedded=X_embed
#    clusters=clusters[::-1]
#    
#    dotsize=1
#    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
#    fig.set_figwidth(12)
#    ax1.scatter(X_embedded[:,0],X_embedded[:,1], alpha=1, s=dotsize)
#    ax1.set_title('t-SNE Embedding of Data', fontsize=20)
#    color = ['red','magenta','orange','lime','black']
#    k = 0
#    slot_x = []
#    slot_y = []
#    cluster_num = []
#    index_num = []
#    for idx, cluster in enumerate( clusters ):
#        if idx in [1,2,3,4]:
#            print(idx)
#            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,label="cluster %s" % (idx), s=dotsize)
#            for i in cluster:
#                slot_x.append(X_embedded[i,0])
#                slot_y.append(X_embedded[i,1])
#                cluster_num.append(idx)
#                index_num.append(i)
#            k+=1
#        else:
#            for i in cluster:
#                slot_x.append(X_embedded[i,0])
#                slot_y.append(X_embedded[i,1])
#                cluster_num.append(-1)
#                index_num.append(i)            
#            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,c = "black",s=dotsize)
#    a = pd.DataFrame()
#    a['x'] = slot_x
#    a['y'] = slot_y
#    a['cluster'] =  cluster_num
#    a['index'] =  index_num
#    a.to_csv('./open-tsne.csv')
#    ax2.set_title("Cluster Labels from DBSCAN", fontsize=20)
#    
#    ax1.axis('off')
#    ax2.axis('off')
#    lgnd = ax2.legend(bbox_to_anchor=(1.04,1), borderaxespad=0, prop={"size":12})
#    for handle in lgnd.legendHandles:
#        handle.set_sizes([40.0])
#    
#    fig.tight_layout()
#    plt.show()
#    fig.savefig("./png/eps:"+str(eps).replace('.','-')+"+min_samples:"+str(min_samples)+".png")
#    plt.close()
#
#aa(2,9)






from openTSNE import TSNE
#dbscan+opentsne
X_embed = X50
X_embedded = TSNE().fit(X50) 
#X_embed_opentsne = TSNE().fit(X50)
#X_embed = X_embed_opentsne
def aa(eps,min_samples):
    clusters = []
    dbscan = DBSCAN(eps, min_samples)#int(dataset.shape[0]*0.015))
    clustering = dbscan.fit( X_embed )
    for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
        c= np.argwhere( clustering.labels_ == i ).flatten().tolist()
        clusters.append( c )
        #print(X_embed)
        #exit()
    
    del dbscan
    del clustering
    
    clusters.sort(key=len)
#    X_embedded=X_embed
    clusters=clusters[::-1]
    
    dotsize=1
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    fig.set_figwidth(12)
    ax1.scatter(X_embedded[:,0],X_embedded[:,1], alpha=1, s=dotsize)
    ax1.set_title('t-SNE Embedding of Data', fontsize=20)
    color = ['red','magenta','orange','lime','black']
    k = 0
    slot_x = []
    slot_y = []
    cluster_num = []
    index_num = []
    for idx, cluster in enumerate( clusters ):
        if idx in [1,2,3,4]:
            print(idx)
            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,label="cluster %s" % (idx), s=dotsize)
            for i in cluster:
                slot_x.append(X_embedded[i,0])
                slot_y.append(X_embedded[i,1])
                cluster_num.append(idx)
                index_num.append(i)
            k+=1
        else:
            for i in cluster:
                slot_x.append(X_embedded[i,0])
                slot_y.append(X_embedded[i,1])
                cluster_num.append(-1)
                index_num.append(i)            
            ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,c = "black",s=dotsize)
    a = pd.DataFrame()
    a['x'] = slot_x
    a['y'] = slot_y
    a['cluster'] =  cluster_num
    a['index'] =  index_num
    a.to_csv('./dbsacn+opentsne.csv')
    ax2.set_title("Cluster Labels from DBSCAN", fontsize=20)
    
    ax1.axis('off')
    ax2.axis('off')
    lgnd = ax2.legend(bbox_to_anchor=(1.04,1), borderaxespad=0, prop={"size":12})
    for handle in lgnd.legendHandles:
        handle.set_sizes([40.0])
    
    fig.tight_layout()
    plt.show()
    fig.savefig("./png/eps:"+str(eps).replace('.','-')+"+min_samples:"+str(min_samples)+".png")
    plt.close()

aa(0.72,83)