# -*- coding: utf-8 -*-
"""
Created on Sun Jan 27 12:01:12 2019

@author: 
"""
"""
#CBOW Word2Vectpr model training with combined Treebank and vulnerability data sets
from cbowSEmbedding import CbowSEmbedding
from text_sequence_input import TextSequenceInput as Input, WordEmbeddingPresenterFactory, RowTextFile as VulnerabilityData, CbowInputPresenter
from posInput import TreebankInput
from training import Trainer
import constants
import numpy as np

def main():
    treeBankPath = r"D:\\Personal\\Emmajack\\Projects\\Corpus\\nltk_data"
    vulnerabilityPath = r"D:\\Personal\\Emmajack\\Projects\\sample-data\\vulnerabilityCorpus_2.txt"
    treeBankCorpus,_ = TreebankInput(treeBankPath).getCorpus()
    vulnerabilityCorpus = VulnerabilityData(vulnerabilityPath).getCorpus()
    corpus = treeBankCorpus + vulnerabilityCorpus
    input = Input(corpus, (.7,.1,.2), WordEmbeddingPresenterFactory, isLabeled=False, includeCharacterEncoding=False)
    dictionarySize = input.getWordDictionarySize()
    wordEmbeddingDimension = constants.VECTOR_DIMENSION
    batchSize = constants.BATCH_SIZE
    windowSize = constants.W2V_WINDOW_SIZE
    print("Word Dictionary: \t\t\t{}".format(dictionarySize))
    print("Word Embedded Vectors Dimension: \t{}".format(wordEmbeddingDimension))
    print("Batch Size: \t\t\t\t{}".format(batchSize))
    print("Context window size: \t\t{}".format(windowSize))
    model = CbowSEmbedding(dictionarySize, wordEmbeddingDimension, window = windowSize).getModel()
    
    trainingPresenter = CbowInputPresenter(
                input.getTrainingData(), #W2V Presenter
                batchSize,
                windowSize, #W2V Context Window
                dictionarySize #The combined dictionary of treebank and vulnerability corpus
            )
            
    validationPresenter = CbowInputPresenter(
                input.getValidationData(),
                batchSize,
                windowSize,
                dictionarySize
            )
    testPresenter = CbowInputPresenter(
                input.getTestData(),
                batchSize,
                windowSize,
                dictionarySize
            )
    epochs = 10

    training = Trainer(trainingPresenter, validationPresenter, testPresenter, model, epochs, statPath="TrainingStat" )   
    training.train()    
    testBatches = testPresenter.nBatches()
    model.evaluate_generator(testPresenter, steps = testBatches)
    embedding = model.get_layer("embedding")
    embeddingWeights = embedding.get_weights()[0]
    np.save(r'TrainedModels\\CBOWWeights.npy', embeddingWeights)
    
"""
"""
#POS Training on Treebank Dataset
import constants
from text_sequence_input import TextSequenceInput as Input
from posModel import PosModel
from posInput import TreebankInput, PosInputPresenterFactory
from training import Trainer


def main():
    # the NTLK Corpora path
    #path=r"D:\\Personal\\Emmajack\\Projects\\Corpus\\nltk_data"
    path = None
    #corpus = RowTextFile(path).getCorpus()
    corpus = TreebankInput(path).getCorpus()
    #input = Input(corpus, False, (.7,.1,.2), WordEmbeddingPresenterFactory)
    input = Input(corpus, (.7,.1,.2), PosInputPresenterFactory, isLabeled=True, includeCharacterEncoding=True)
    
    wordDictionarySize = constants.VULNERABILITY_TREEBANK_DICTIONARY_SIZE #input.getWordDictionarySize() 
    charDictionarySize = input.getCharacterDictionarySize()
    posCategories = input.getLabelDictionarySize() 
    
    maxSentenceLength = input.getMaxSequenceLength()
    maxWordLength = input.getMaxWordLength()
    
    wordEmbeddingWeightsPath = "TrainedModels/CBOWWeights.npy"
    wordEmbeddingDimension = constants.VECTOR_DIMENSION
    characterEmbeddingDimension = constants.CHARACTER_VECTOR_DIMESION
    
    posFeaturesLstmUnits = constants.POS_FEATURE_LSTM_UNITS
    posTagsPredictingLstmUnits = constants.POS_TAGS_DEPENDENCY_LSTM_UNITS
    posTagsDependencyLstmUnits = constants.POS_TAGS_DEPENDENCY_LSTM_UNITS
    

    print("Word Dictionary: \t\t\t{}".format(wordDictionarySize))
    print("Character Dictionary: \t\t\t{}".format(charDictionarySize))
    print("POS Tags: \t\t\t\t{}".format(posCategories))
    print("Longest Sentence: \t\t\t{}".format(maxSentenceLength))
    print("Longest Word: \t\t\t\t{}".format(maxWordLength))
    print("Word Embedded Vectors Dimension: \t{}".format(wordEmbeddingDimension))
    print("Character Embedded Vectors Dimension: \t{}".format(characterEmbeddingDimension))
    print("POS Features LSTM Units: \t\t{}".format(posFeaturesLstmUnits))
    print("POS Tags Predicting LSTM Units: \t{}".format(posTagsPredictingLstmUnits))
    print("POS Tags Dependency LSTM Units: \t{}".format(posTagsDependencyLstmUnits))
    
    trainingPresenter = input.getTrainingData()
    validationPresenter = input.getValidationData()
    testPresenter = input.getTestData()
    epochs = 150
    
    
    model = PosModel(wordEmbeddingDimension, maxSentenceLength, maxWordLength,
                     wordDictionarySize, wordEmbeddingWeightsPath, charDictionarySize,
                     characterEmbeddingDimension, posFeaturesLstmUnits,
                     posTagsPredictingLstmUnits, posTagsDependencyLstmUnits, posCategories).getModel()

    training = Trainer(trainingPresenter, validationPresenter, testPresenter, 
                       model, epochs, statPath="TrainingStat", accName = "new_sparse_categorical_accuracy")   
    training.train()
    # serialize model to JSON
    model_json = model.to_json()
    with open("TrainedModels/pos_model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("TrainedModels/pos_model.h5")
    
    # save also the partitioned dataset to file so that evaluation is done on the saved models
    input.save("Dataset/pos_data_set")
    
"""

"""
#Loading trained model and evaluate
import sys
import os
import numpy as np
import constants
from text_sequence_input import TextSequenceInput as Input
from posModel import PosModel
from trainedPosModel import TrainedModel
from posInput import TreebankInput, PosInputPresenter, PosInputPresenterFactory
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc

def main():
    # the NTLK Corpora path
    # path = None
    path = r"D:\\Personal\\Emmajack\\Projects\\Corpus\\nltk_data"
    #corpus = RowTextFile(path).getCorpus()
    corpus = TreebankInput(path).getCorpus()
    #input = Input(corpus, False, (.7,.1,.2), WordEmbeddingPresenterFactory)
    input = Input(corpus, (.7,.1,.2), PosInputPresenterFactory, isLabeled=True, includeCharacterEncoding=True)
    
    testPresenter = input.getTestData()
    testBatches = testPresenter.nBatches()
    
    model = TrainedModel(r"TrainedModels\\pos_model", "adam", "sparse_categorical_crossentropy",[acc]).getModel()

    testResult = model.evaluate_generator(testPresenter, steps = testBatches)
    print(testResult)

"""
import json

def main():
    with open('Dataset/pos_data_set_label_dictionary.txt', 'r') as jsonFile:
        jsonDictionary = json.load(jsonFile)
    
    posTagDictionary, posTagReverseDictionary = tuple(json.loads(jsonDictionary))
    
    posTags = [x for x in posTagDictionary.keys()]
    print(len(posTags));

if __name__ == '__main__':
    main()
