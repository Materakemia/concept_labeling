# -*- coding: utf-8 -*-
"""

@author: 
"""

from keras.models import model_from_json
from keras.models import Model as md
from interfaces import ModelProvider

class TrainedModel(ModelProvider):
    '''
    The model loads the structure and trained weights from file.
    We are assuming that the structure is in json and the weights are in h5
    files. Both have the same name and share the same path
    '''
    def __init__(self, modelfile, optimizer, loss, metrics):
        file = open(modelfile+".json", "r")
        model_json = file.read()
        model = model_from_json(model_json)
        model.load_weights(modelfile+".h5")
        super().__init__(model)
        
    def compile(self, optimizer, loss, metrics):
        super().getModel().compile(optimizer, loss, metrics)
    