# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 19:55:55 2021

@author: 
"""
import numpy as np
import matplotlib.pyplot as plt
import constants
from autoLabelerInput import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory
from AutoLablerGveaModel import AutoLabelerGvaeModel

def train(
        path, # spefify the path to the xml file containing the selected for training noun-phrases
        labelPath # specify the path to the csv file containing noun-phrase to label mapping
        ):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 500
    M = 8
    N = 100

    epochs = 50
    batch_size = constants.BATCH_SIZE

    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=500)
    inData.initializeEmptyPathInput()

    (x_train, _), (x_test, y_test) = _getLabeledInput(inData)
    
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # train the auto-labeler
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    model = provider.getModel()
    encoder = provider.getEncoder()
    for e in range(epochs):
        model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
                validation_data=(x_test, x_test))
        provider.updateTau(e)
        
    abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
    rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
    encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_test)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_test)
    plt.colorbar()
    plt.show()
    plt.savefig('Training_Model.jpg')
    
    autolabelerEncoderModelFileName = "ALEncoder_model"
    _saveModelWeights(encoder, autolabelerEncoderModelFileName)    
    inData.save('Dataset/autolabeler_')

def evaluate(path, labelPath):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 500
    M = 8
    N = 100
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=500)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()
    plt.savefig('Evaluation_Model.jpg')

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):
    # serialize weights to HDF5
    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")

    pass
if __name__ == '__main__':
    # train('Dataset/gazette_data_set_relative_path_noun_phrases_Labled.xml', 'Dataset/20201129_LabledEntities.csv')
    # first train the model
    #train('PATH-TO-THE-EXTRACTED-SAMPLE-XML-DATA', 'PATH-TO-THE-CORRESPONDING-LABELS-CSV-DATA')

    # uncomment the folowing and comment the above
    # evaluate with manually labeled data
    evaluate('Dataset/gazette_data_set_relative_path_noun_phrases_Labled.xml', 'Dataset/20201129_LabledEntities.csv')
