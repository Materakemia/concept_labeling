# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 06:32:03 2021

@author: 
"""

import numpy as np
from numpy import savetxt
from numpy import asarray

import matplotlib.pyplot as plt

from tensorflow.keras.layers import Input, Dense, Lambda, Concatenate, Reshape
from tensorflow.keras.models import Model
from tensorflow.keras import backend as K
from tensorflow.keras.losses import binary_crossentropy as bce

from interfaces import ModelProvider
import constants
  
class AutoLabelerGvaeModel(ModelProvider):
    def sampling(self, inputs):
        logits_y, tau = inputs
        N = self._N
        M = self._M
        U = K.random_uniform(K.shape(logits_y), 0, 1)
        y = logits_y - K.log(-K.log(U +1e-20) + 1e-20)
        y = K.reshape(y, (-1, N, M))/tau
        y = K.softmax(y)
        y = K.reshape(y, (-1, N * M))
        return y

    def __init__(self,absDictionarySize, relDictionarySize, noOfGazetteEntities, intermediate_dim, latent_dim, M, N ):
        self._M = M
        self._N = N
        self._absTau = K.variable(5.0, name='absTemprature')
        self._relTau = K.variable(5.0, name='relTemprature')
        self._anneal_rate = 0.0003
        self._min_temprature = 0.5
        
        # encoding layers
        self.abs_x = Input(shape=(absDictionarySize,))
        self.rel_x = Input(shape=(noOfGazetteEntities, relDictionarySize))
 
        abs_x = Input(shape=(absDictionarySize,))
        rel_x = Input(shape=(noOfGazetteEntities, relDictionarySize))
        
        abs_intermediate_layer = Dense(intermediate_dim, activation='relu')
        abs_latent_layer = Dense(latent_dim, activation='relu') 
        abs_logits_y_layer = Dense(M*N)

        rel_intermediate_layer = Dense(intermediate_dim, activation='relu')
        rel_latent_layer = Dense(latent_dim, activation='relu') 
        rel_logits_y_layer = Dense(M*N)

        abs_intermediate = abs_intermediate_layer(abs_x)
        abs_latent = abs_latent_layer(abs_intermediate)
        abs_logits_y = abs_logits_y_layer(abs_latent)
        
        rel_logits_y_list = []
        for i in range(noOfGazetteEntities):
            component = Lambda(lambda x: x[:,i,:])(rel_x)
            rel_intermediate = rel_intermediate_layer(component)
            rel_latent = rel_latent_layer(rel_intermediate)
            rel_logits_y_list.append(rel_logits_y_layer(rel_latent))
        
        rel_logits_y = Concatenate(axis=1)(rel_logits_y_list)
        
        encoder = Model([abs_x, rel_x], [abs_logits_y, rel_logits_y])

        # decoder layers
        abs_z = Lambda(self.sampling, output_shape=(M*N,))([abs_logits_y, self._absTau])
        rel_z = []
        for logit_y in rel_logits_y_list:
            rel_z.append(Lambda(self.sampling, output_shape=(M*N,))([logit_y, self._relTau]))
        
        # latent_input = Input(shape=(M*N, ))
        abs_decoder_latent_layer = Dense(latent_dim, activation='relu')
        abs_decoder_intermediate_layer = Dense(intermediate_dim, activation='relu')
        abs_decoder_output_layer = Dense(absDictionarySize, activation='sigmoid')

        rel_decoder_latent_layer = Dense(latent_dim, activation='relu')
        rel_decoder_intermediate_layer = Dense(intermediate_dim, activation='relu')
        rel_decoder_output_layer = Dense(relDictionarySize, activation='sigmoid')
        
        abs_decoded_latent = abs_decoder_latent_layer(abs_z)
        abs_decoded_intermediate = abs_decoder_intermediate_layer(abs_decoded_latent)
        abs_decoded_x = abs_decoder_output_layer(abs_decoded_intermediate)
        
        rel_decoded_x_list = []
        for rel_z_component in rel_z:
            rel_decoded_latent = rel_decoder_latent_layer(rel_z_component)
            rel_decoded_intermediate = rel_decoder_intermediate_layer(rel_decoded_latent)
            rel_decoded_x_component = rel_decoder_output_layer(rel_decoded_intermediate)
            rel_decoded_x_list.append(Reshape((1,relDictionarySize))(rel_decoded_x_component))
        
        rel_decoded_x = Concatenate(axis=1)(rel_decoded_x_list)
        
                                      
        # defining the loss function
        # for the absolute component
        abs_q_y = K.reshape(abs_logits_y, (-1, N, M))
        abs_q_y = K.softmax(abs_q_y)
        abs_log_q_y = K.log(abs_q_y + 1e-20)
        abs_kl_tmp = abs_q_y * (abs_log_q_y - K.log(1.0/M))
        abs_KL = K.sum(abs_kl_tmp, axis=(1,2))
        
        #for the relative component
        rel_q_y = K.reshape(rel_logits_y, (-1, noOfGazetteEntities*N, M))
        rel_q_y = K.softmax(rel_q_y)
        rel_log_q_y = K.log(rel_q_y + 1e-20)
        rel_kl_tmp = rel_q_y * (rel_log_q_y - K.log(1.0/M))
        rel_KL = K.sum(rel_kl_tmp, axis=(1,2))
        
        rel_x_l = K.reshape(rel_x, (-1, noOfGazetteEntities*relDictionarySize))
        x_l = K.concatenate([abs_x, rel_x_l])
        
        rel_decoded_x_l = K.reshape(rel_decoded_x,(-1,noOfGazetteEntities*relDictionarySize))
        decoded_x_l = K.concatenate([abs_decoded_x, rel_decoded_x_l])
        elbo = (absDictionarySize+relDictionarySize)*bce(x_l, decoded_x_l) - (abs_KL+rel_KL)
        # elbo = absDictionarySize * bce(abs_x, abs_decoded_x) - (abs_KL+rel_KL)

        # elbo = absDictionarySize * bce(abs_x, abs_decoded_x) + relDictionarySize * bce(rel_x, rel_decoded_x) - (abs_KL+rel_KL)

        # defining the autoencoder model
        vae = Model([abs_x, rel_x], [abs_decoded_x, rel_decoded_x])
        vae.add_loss(elbo)
        vae.compile(optimizer='adam')
        print(vae.summary())

        self._encoder = encoder
        super().__init__(vae)
    
    def getX(self):
        return self.abs_x;
    
    def getEncoder(self):
        return self._encoder;
    
    def updateTau(self, e):
        K.set_value(self._absTau, 
                    np.max([K.get_value(self._absTau) * np.exp(-self._anneal_rate*e), self._min_temprature]))
        K.set_value(self._relTau, 
                    np.max([K.get_value(self._relTau) * np.exp(-self._anneal_rate*e), self._min_temprature]))
    
def main():
    # obtain training data
    import mixedModeRandomData as mmr
    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50

    epochs = 1
    batch_size = constants.BATCH_SIZE
    
    # absDictionarySize = 5000
    # relDictionarySize = 4000
    
    # (x_train, y_train),(x_test, y_test) = mmr.generateMixedBinomDistributionData(absDictionarySize=absDictionarySize, relDictionarySize=relDictionarySize, sampleSize=10000, noiseSize=0)
    
    import autoLabelerInput as inputs
    inputPath = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample.xml'
    labelPath = 'Dataset/20201129_LabledEntities.csv'
    (dictAbs,dictRel),(x_train,y_train),(x_test, y_test),(x_test_abs, x_test_rel) = inputs.getLabeledInput(inputPath, labelPath, pathVocabularySize=500)
    absDictionarySize = dictAbs
    relDictionarySize = dictRel
    #print(x_train)
    #x_train_array = asarray(x_train)
    #savetxt( 'TuneTrainingStat/x_train.csv', x_train_array )
 
    #exit(1)
        # train the auto-labeler
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    model = provider.getModel()
    encoder = provider.getEncoder()
    abs_x = provider.getX()
    for e in range(epochs):
        model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
                validation_data=(x_test, x_test))
        provider.updateTau(e)
        
    #abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
    #rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
    #encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))
    
 
    abs_encoded_test, rel_encoded_test = encoder.predict(x_test)
    rel_encoded_test = np.reshape(rel_encoded_test, (-1,4*M*N))
    encoded_test = np.hstack((abs_encoded_test,rel_encoded_test))
 
    savetxt( 'TuneTrainingStat/abs_encoded_test.csv', abs_encoded_test)
    savetxt( 'TuneTrainingStat/rel_encoded_test.csv', rel_encoded_test)
    savetxt( 'TuneTrainingStat/encoded_test.csv', encoded_test)
    print(encoded_test)
   
    def get_code(item):
        if 1 in item.tolist(): 
            return item.tolist().index(1)
        else:
            return -1

    with open('Dataset/x_test_abs.csv', 'w') as fp:
        fp.write('%s,%s\n' % ('index', 'code'))
        i=0;
        for item in x_test_abs:
            fp.write('%d,%d\n' % (i,get_code(item)))
            i = i+1    
            
    with open('Dataset/x_test_rel.csv', 'w') as fp:
        fp.write('%s,%s\n' % ('index', 'code'))
        i=0;
        for item in x_test_rel:
            fp.write('%d,%s,%d\n' % (i,item, get_code(item)))
            i = i+1
    
    #with open('x_test.csv', 'w') as fp:
        #i=0;
        #for l in x_test[0]:
           # fp.write('%d, %s\n' % (i,l))
           # i = i+1
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
   
    abs_viz = tsne.fit_transform(abs_encoded_test)
    rel_viz = tsne.fit_transform(rel_encoded_test)
    #viz = tsne.fit_transform(encoded_test)
    
    #print(inputs.getLabeledInput(inputPath, labelPath, pathVocabularySize=500))
    # print(encoded_test)
    #print(viz)
   # print(viz)
    
   
    with open('Dataset/abs_viz.csv', 'w') as fp:
        fp.write('%s,%s,%s\n' % ('index', 'x','y'))
        i=0;
        for l in abs_viz:
            fp.write('%d,%s,%s\n' % (i,l[0],l[1]))
            i = i+1
               
    with open('Dataset/rel_viz.csv', 'w') as fp:
        fp.write('%s,%s,%s\n' % ('index', 'x','y'))
        i=0;
        for l in rel_viz:
            fp.write('%d,%s,%s\n' % (i,l[0],l[1]))
            i = i+1
            
   # with open('Dataset/viz.csv', 'w') as fp:
      # fp.write('%s, %s, %s\n' % ('index', 'x','y'))
       # i=0;
       # for l in viz:
           # fp.write('%d, %s, %s\n' % (i,l[0],l[1]))
           # i = i+1
                     
    plt.figure(figsize=(6,6))
    plt.scatter(abs_viz[:,0], abs_viz[:, 1],c=y_test)
    plt.colorbar()
    plt.show()
    
    #plt.figure(figsize=(6,6))
   # plt.scatter(viz[:,0], viz[:, 1],c=y_test)
    #plt.colorbar()
    #plt.show()
    
if __name__ == '__main__':
    main()            