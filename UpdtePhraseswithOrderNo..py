# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 11:27:06 2021

@author: 
"""
import numpy as np
from xml.etree import ElementTree
from bs4 import BeautifulSoup as bs


path = 'Dataset/gazette_data_set_relative_path_noun_phrases.xml'
pathOut = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample.csv'
root = ElementTree.parse(path).getroot()


for sentence in root.findall('sentences/sentence'):
    orderNumber = 1
    for entity in sentence.findall('entities/entity'):
        entity.set('OrderNo', str(orderNumber))
        orderNumber = orderNumber + 1

mydata = ElementTree.tostring(root).decode("utf-8") 
myfile = open("gazette_data_set_relative_path_noun_phrases.xml", "w")
myfile.write(mydata)
myfile.close()