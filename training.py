# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 20:12:37 2019

@author: 
"""
from abc import ABC
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from keras.utils import plot_model

class TrainingStatstics(ABC):
    def __init__(self, history,modelName, statPath, accName = 'acc'):
        self._modelPath = statPath + r'/' + modelName + '.png'
        self._filePath = statPath + r'/'+ modelName + '.csv'
        metrics = history.history.keys()
        self._loss = history.history['loss']
        self._accuracy = None
        if(accName in metrics):
            self._accuracy = history.history[accName]
        self._validationLoss = None
        if('val_loss' in metrics):
            self._validationLoss = history.history['val_loss']
        self._validationAccuracy = None
        valAcc = 'val_{}'.format(accName)
        if(valAcc in metrics):
            self._validationAccuracy = history.history[valAcc]
    
    def save(self):
        records = len(self._loss)
        with open(self._filePath, 'w') as f:
            f.write("loss,acc,val_loss,val_acc\n")
            f.write("{},{},{},{}\n".format(
                records,
                records if self._accuracy is not None else 0,
                records if self._validationLoss is not None else 0,
                records if self._validationAccuracy is not None else 0))
            for i in range(0,len(self._loss)):
                line="{},{},{},{}\n".format(
                    self._loss[i], 
                    self._accuracy[i] if self._accuracy is not None else '',
                    self._validationLoss[i] if self._validationLoss is not None else '',
                    self._validationAccuracy[i] if self._validationAccuracy is not None else '')
                f.write(line)
            f.flush()
        
    def load(self):
        with open(self._filePath, 'r') as f:
            f.readline() #descard headers
            metricsSizes = f.readline().split(',')
            hasAccuracy = metricsSizes[1] is not '0' 
            hasValLoss = metricsSizes[2] is not '0'
            hasValAcc = metricsSizes[3] is not '0'
            self._loss = []
            self._accuracy = [] if hasAccuracy else None
            self._validationLoss = [] if hasValLoss else None
            self._validationAccuracy = [] if hasValAcc else None
            for line in f:
                values = line.split(',')
                self._loss.append(float(values[0]))
                if(hasAccuracy):
                    self._accuracy.append(float(values[1]))
                if(hasValLoss):
                    self._validationLoss.append(float(values[2]))
                if(hasValAcc):
                    self._validationAccuracy.append(float(values[3]))
                
    def plot(self):
        epochs = range(1, len(self._loss) + 1)
        if(self._accuracy is not None):
            plt.plot(epochs, self._accuracy, 'bo', label='Training acc')
            title = "Training{}{} accuracy"
            formatIn = []
            if(self._validationAccuracy is not None):
                plt.plot(epochs, self._validationAccuracy, 'b', label='Validation acc')
                formatIn.extend([" and", " validation"])
            else:
                formatIn=['','']
            plt.title(title.format(*formatIn))
            plt.legend()
            plt.figure()
            
        plt.plot(epochs, self._loss, 'bo', label='Training loss')
        title = "Training"
        if(self._validationLoss is not None):
            plt.plot(epochs, self._validationLoss, 'b', label='Validation loss')
            title += " and validation"
        title += " loss"
        plt.title(title)
        plt.legend()
        plt.show()
        
    def saveModelVis(self, model):
        plot_model(model, to_file=self._modelPath, show_shapes=True)

class Trainer(ABC):
    def __init__(self, trainingPresenter, validationPresenter, 
                 testPresenter, model, epochs, statPath=None, accName = 'acc'):
        self._trainingPresenter = trainingPresenter
        self._validationPresenter = validationPresenter
        self._testPresenter = testPresenter
        self._model = model
        self._epochs = epochs
        self._statPath = statPath if statPath is not None else '.'
        self._accName = accName
        super().__init__()
        
    def train(self):
        tPresenter = self._trainingPresenter
        vPresenter = self._validationPresenter
        tBatches = tPresenter.nBatches()
        vBatches = vPresenter.nBatches()
        model = self._model
        # view model summary
        print(model.summary())
        
        history = model.fit_generator(tPresenter, steps_per_epoch=tBatches,
                                  validation_data=vPresenter, 
                                  validation_steps=vBatches,
                                  epochs=self._epochs)
        
        statstics = TrainingStatstics(history, model.name, self._statPath, accName=self._accName)
        statstics.saveModelVis(model)
        statstics.save()
        statstics.plot()
            
