# -*- coding: utf-8 -*-
"""


@author: 
"""

#Loading trained model and evaluate

from text_sequence_input import RowTextFile
from trainedPosModel import TrainedModel
from posEvaluationInput import PosEvaluationInput as Input, PosPredictionPresenterFactory, LabledTextFile
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc
import numpy as np
import json

def main():
    #loading the evaluation sample data
    path = 'Corpus/190601_Corpus.txt'
    evaluationSample = RowTextFile(path).getCorpus()
    evaluationInput = Input(False, evaluationSample, "Dataset/pos_data_set_tuned",  24, 271, PosPredictionPresenterFactory)
    presenter = evaluationInput.getPresenter()
    
    with open("Dataset/pos_data_set_tuned_label_dictionary.txt", 'r') as jsonFile:
        jsonDictionary = json.load(jsonFile)
        
    dictionary = tuple(json.loads(jsonDictionary));
    
#    x = presenter.__next__()[0]
    steps = presenter.nBatches()
        
    model = TrainedModel("TrainedModels/pos_model_tuned", "adam", "sparse_categorical_crossentropy",[acc]).getModel()
    model.compile("adam", "sparse_categorical_crossentropy", [acc])
    
 
    predictionResult = model.predict_generator(presenter, steps = steps)    
    sentenceResult =[[np.where(word == np.amax(word))[0][0] for word in sentence] for sentence in predictionResult]
      
    sen = 0
    wor = 0    
    file = open("Dataset/pos_data_set_tuned_tagged_corpus.txt", 'w+')
    for sentence in evaluationSample:
        if (len(sentence) > 0):
            for word in sentence:
                if( wor < 270):
                    file.write('[' + str(sen + 1) + '](' + word + ',' + (dictionary[1])[str(sentenceResult[int(sen)][int(wor)])] + ')\n')
                    wor = wor + 1
            sen = sen + 1 
            wor = 0  
    file.close()
    
#    print(sentenceResult)
    
#    for sentenece 
    
    
#    y = evaluationResult[0][2]
#    print(evaluationResult[0])
#    print("Loss=" ,evaluationResult[0], " Accu=" ,evaluationResult[1])
#    print(model.metrics_names)

if __name__ == '__main__':
    main()

