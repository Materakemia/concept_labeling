# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 23:01:35 2020

@author: 
"""
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.dom import minidom
import re
import unicodedata
import csv

csv_path = 'Dataset/20201129_LabledEntities.csv'
xml_path = 'Dataset/gazette_data_set_relative_path_noun_phrases.xml'
pathOut = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample.xml'

csv_list = []
with open(csv_path, 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    csv_list = ([item[0] for item in list(spamreader)][1:])

sentence_list_int = [int(i) for i in csv_list]
sentence_list = list(set(sentence_list_int))

tree = ET.parse(xml_path)
root = tree.getroot()

for sentences in root.findall('sentences'):
    for sentence in sentences.findall('sentence'):
        print(sentence.find('id').text)
        if int(sentence.find('id').text) in sentence_list:
            for entities in sentence.findall('entities'):
                order_no = 1
                for entity in entities.findall('entity'):
                    entity.set('OrderNo', str(order_no))
                    order_no = order_no +1 
        else:
            sentences.remove(sentence)


tree.write(pathOut)