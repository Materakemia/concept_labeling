import nltk.parse.stanford as parser
from posInput import PosInputPresenterFactory
from training import Trainer
from training import TrainingStatstics as ts
from text_sequence_input import TextSequenceInput as Input
from keras import models, optimizers
from keras.models import Sequential
from posEvaluationInput import LabledTextFile
from gazetteInput import PhraseEnumerator as pe
import os 
import re
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.dom import minidom
from xml.etree import ElementTree

import time

java_path = "C:/Program Files/Java/jre1.8.0_191/bin/java.exe"
os.environ['JAVAHOME'] = java_path
os.environ['CLASSPATH'] = 'stanford-parser/stanford-parser-3.9.2-models.jar'
jarPath = 'stanford-parser/stanford-parser.jar'

path = 'DataSet/pos_data_set_tuned_tagged_corpus.txt'
pathNounPhrases = 'DataSet/pos_data_set_tuned_tagged_corpus_noun_phrases.xml'

stanfordparser = parser.StanfordParser(jarPath)

corpus = LabledTextFile(path).getCorpus()
lists_ = list(map(lambda x, y:(x,y), corpus[0], corpus[1]))

merged_list = []
index = 1
sentId = 1
file = open(pathNounPhrases, 'w')
batch_size = 10
then = time.time()
for combo in lists_:
    merged_list.append([(combo[0][i], combo[1][i]) for i in range(0, len(combo[0]))])
    index+=1
    if(index % batch_size == 0):
        otree = stanfordparser.tagged_parse_sents(merged_list)
        parsing = Element('parsing')
        sentences = SubElement(parsing, 'sentences')
        for tree in otree:
            sentence = SubElement(sentences, 'sentence')
            sentenceId = SubElement(sentence, 'id')
            sentenceId.text = str(sentId)
            nounPhrasesNode = SubElement(sentence, 'nounphrases')            
            s = []
            s = list(tree)
            lines = (''.join(map(str, s))).splitlines()
            mystr = ' '.join([line.strip() for line in lines])
            mystrC = re.sub("\s\(", "(", mystr)
            parsed = pe(mystrC)
            nounPhrases = parsed.getCollectedNounPhrases()
            for path, phrase in nounPhrases:
                nounPhrase = SubElement(nounPhrasesNode, 'nounphrase')
                pathNode = SubElement(nounPhrase, 'path')
                pathNode.text = path
                phraseNode = SubElement(nounPhrase, 'phrase')
                phraseNode.text = phrase       
            sentId+=1
        rough_string = ElementTree.tostring(parsing, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        reparsedString = reparsed.toprettyxml(indent="  ")
        file.write(reparsedString)
        now = time.time()
        print(str(sentId) + ": " + str(now-then) + "seconds")
        merged_list.clear()

#otree = stanfordparser.tagged_parse_sents(merged_list)
#
#sentences = []
#lstText = []
#nounPhrasesCorpus = []
#
#index = 1
#parsing = Element('parsing')
#sentences = SubElement(parsing, 'sentences')
#
#for combo in lists_:
#    sentence = SubElement(sentences, 'sentence')
#    sentenceId = SubElement(sentence, 'id')
#    sentenceId.text = str(index)
#    nounPhrasesNode = SubElement(sentence, 'nounphrases')
#    merged_list = []
#    merged_list.append([(combo[0][i], combo[1][i]) for i in range(0, len(combo[0]))])
#    otree = stanfordparser.tagged_parse_sents(merged_list)
##    res = stanfordparser._parse_trees_output(otree)
##    print(merged_list)
#    for tree in otree:
#        s = []
#        s = list(tree)
#        lines = (''.join(map(str, s))).splitlines()
#        mystr = ' '.join([line.strip() for line in lines])
#        mystrC = re.sub("\s\(", "(", mystr)
#        parsed = pe(mystrC)
##        phrases = parsed.getPhrases()
#        nounPhrases = parsed.getCollectedNounPhrases()
#        for path, phrase in nounPhrases:
#            nounPhrase = SubElement(nounPhrasesNode, 'nounphrase')
#            pathNode = SubElement(nounPhrase, 'path')
#            pathNode.text = path
#            phraseNode = SubElement(nounPhrase, 'phrase')
#            phraseNode.text = phrase                
#        nounPhrasesCorpus.append(nounPhrases)
#    print(str(index))
#    index+=1
#    
##    if(index == 10):
##        break
#file = open(pathNounPhrases, 'w')
#rough_string = ElementTree.tostring(parsing, 'utf-8')
#reparsed = minidom.parseString(rough_string)
#reparsedString = reparsed.toprettyxml(indent="  ")
#file.write(reparsedString)

#def main():
#    #loading the evaluation sample data
#
#
#
#
#
##print(parser.GenericStanfordParser.tagged_parse([('hello', 'NN'),('hello', 'NN'),('hello', 'NN'),('hello', 'NN'),('hello', 'NN')]))
#
#if __name__ == '__main__':
#    main()
