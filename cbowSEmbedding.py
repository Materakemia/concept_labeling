# -*- coding: utf-8 -*-
"""

@author: 
"""

import keras.backend as K
from keras.models import Model
from keras.layers import Dense, Embedding, Lambda, Input, dot
from interfaces import ModelProvider
class CbowSEmbedding(ModelProvider):
    '''
    The variant of CBOW Word2Vector model is trained to predict whether or not
    the predicted from input context words and the target are the same by computing
    their dot products. Labels are of value 0 and 1 depending on the input context
    words are negative or positive samples. The target word is padded to the same
    size as context words: np.array(c1,c2,c3,c4) and target word: np.array([t,0,0,0])
    for window size of 2
    '''
    def __init__(self, dictionarySize, embeddingDimension, window=2, padding = True):
        
        # We shall use the keras functional api to build the model
        
        # The input variables
        context_samples = Input(shape=(2*window,), name="Context")
        targets = Input(shape=(2*window,), name="Target")
        
        # Creating the embedding layer
        embedding = Embedding(dictionarySize+1,embeddingDimension, mask_zero=padding, name='embedding')
        
        # Computing the embedded vectors corresponding to both the context_samples and the target 
        vec_context_samples = embedding(context_samples)
        vec_targets = embedding(targets)
        
        # Averaging layer 
        mean = Lambda(lambda x: K.mean(x, axis=1), name="Mean")
        
        # The mean of context samples embedded vectors
        mean_samples = mean(vec_context_samples)
        
        # We should have non-zero vector for the first target components as 
        # the remaining components are paddings the mean should provide the target
        # embedded vector
        mean_targets = mean(vec_targets)
        
        # Computing the cosine of the angle between the context and target vectors
        dot_product = dot([mean_samples, mean_targets], axes=1, normalize=True, name="Similarity")
        
        # For negative samples the dot_product should be 0 and 1 for positive ones
        output = Dense(1, activation='sigmoid', name="Binary-Clasification")(dot_product)
        
        # Defining the model with context samples and the target tensors
        model=Model(inputs=[context_samples,targets], output=output, name="CBOW-W2V-Similarity")
        
        # Compile with the 'binary_crossentropy' loss and 'rmsprop' optimizer
        model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['acc'])
        
        super().__init__(model)
        
