# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 12:26:27 2019
@author: 
"""
#################################
import argparse as arg
import csv
import os
import re
import requests
from io import BytesIO as BIO
from zipfile import ZipFile as ZF
import xml.etree.ElementTree as ET

from bs4 import BeautifulSoup
from urllib.parse import urlparse
import urllib.request as urrqs
#################################

class ArgumentManager:
    def argument_parser():
        try:
            usage_msg = '''InputRasterData.py [-h] url tags.csv
    
            Locate the url and download and extract the information from all tags indicated in tags.csv
            Example: InputRasterData.py "https://nvd.nist.gov/vuln/data-feeds#XML_FEED" "tags.csv" "version" "OutputFile"
            '''
            parse = arg.ArgumentParser(usage=usage_msg)
            parse.add_argument('url', help="url of the site", type=str)
            parse.add_argument('tags_container', help="csv file with tags to be extracted", type=str)
            parse.add_argument('version', help="Specific Version to be Downloaded. If no version to specify use '/'", type=str)
            parse.add_argument('output_destination', help="Output File Name", type=str)
    
            args = parse.parse_args()            
            os.chdir(os.path.dirname(os.path.realpath(__file__)))
            
        except Exception as ex:
            parse.error(usage_msg)
            print('error:? ' + str(ex))
            print('Please use <InputRasterData.py --help> for more information')
        return args.url, args.tags_container, args.version, args.output_destination

    def csv_reader(tags_file):
        list_of_tags = []
        if os.path.isfile(tags_file):
            with open(str(tags_file)) as csvfile:
                read_csv = csv.reader(csvfile, delimiter=',')
                list_of_tags = list(read_csv)
        return list_of_tags
    
class WebScraper:
    
    def getHostName(url):
            #get host name
        parsed_uri = urlparse(url)
        host_name = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
        return host_name
    
    def getPageContent(url):
        html_page = urrqs.urlopen(url)
        #parse page using soup
        page_content = BeautifulSoup(html_page, 'html.parser')#html.parser should be changed to arg so that another parsers could be used
        return page_content
    
    def getXmlZipLinks(url, host_name, page_content, version):
        #open page
       request_urls = [] 
       url = ""
       for link in page_content.findAll('a', attrs={'href': re.compile("xml.zip")}):#xml.zip should be changed to arg so that another filenames could be included
          if host_name in link.get('href'):
              url = link.get('href')
          else:
              url = host_name + link.get('href')
              
          if version not in url:
               continue
          request_urls.append(url) 
       print(request_urls)   
       return request_urls
#   def getConsecutiveLinksFromWebSite(url, pageContent):
#       request_urls = []
       

    
class FileDownloader(WebScraper):
                  
    def downloadFileToDirectory(url):
        #obtain filename by splitting url and getting last string
        file_name = url.split('/')[-1]   
        #create response object
        r = requests.get(url, stream = True)
        #download started
        with open(file_name, 'wb') as f:
            for chunk in r.iter_content(chunk_size = 1024*1024):
                if chunk:
                    f.write(chunk)      
        print("%s downloaded!"%file_name)
            
    def downloadFileToMemory(url):  
        request = requests.get(url)
        file = BIO() 
        file.write(request.content) 
        return file
    
    def downloadZipFileToMemory(url):
        request = requests.get(url)
        zip_data = BIO()
        zip_data.write(request.content)
        zip_file = ZF(zip_data)       
        return zip_file
            
class FileExtractor(FileDownloader):
    def extractZipFile(file):   
        file = BIO()
        file = ZF(file)
        return {i: file.read(i) for i in file.namelist()}

class CorpusManager(FileDownloader):
    def corpusWriter(request_urls, tag_list, name_spaces, output_file):
        for urls in request_urls:
            print(urls)
            zip_file = FileDownloader.downloadZipFileToMemory(urls)
            for i in zip_file.namelist():
                text_xml = zip_file.read(i).decode('UTF-8', 'ignore')
                root = ET.fromstringlist(text_xml)
                corpusFile = open(output_file, "a+", encoding='utf-8')
                for tag in tag_list:
                    for index in tag:
                        for ns in name_spaces:                 
                            for key, value in ns.items():
                                for vuln_tag in root.iter():
                                    if (vuln_tag.tag == ('{' + value + '}' + index)):
                                        if not str(vuln_tag.text):
                                            continue
                                        corpusFile.write(str(vuln_tag.text))
                                        corpusFile.write("--end--")


def main():
    url, tags_file, version, output_file = ArgumentManager.argument_parser()
    
    tag_list = []    
    tag_list = ArgumentManager.csv_reader(tags_file)
       
    name_spaces = []
    name_spaces = [{'vuln':'http://scap.nist.gov/schema/vulnerability/0.4'}, {None: 'http://cwe.mitre.org/cwe-6'}, {None:"http://capec.mitre.org/capec-3"}, {'xhtml':'http://www.w3.org/1999/xhtml'}]
      
    host_name = WebScraper.getHostName(url)
    page_content = WebScraper.getPageContent(url)
    request_urls = []
    request_urls = WebScraper.getXmlZipLinks(url, host_name, page_content, version)
    CorpusManager.corpusWriter(request_urls, tag_list, name_spaces, output_file)    


if __name__ == '__main__':
    main()
